/////////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab01a - Hello World - EE 491 - Spr 2022
///
/// @file    hello.c
/// @version 1.0 - Initial version
///
/// "Hello World!" programs are the most basic program that can be written and
/// are used to demonstrate the correct operation of the edit-compile-run
/// toolchain.
///
/// @author  Will Bracken<@todo wbracken@hawaii.edu>
/// @date    11_Jan_2022
///
/// @see     https://en.wikipedia.org/wiki/%22Hello,_World!%22_program
/// @see     https://www.thesoftwareguild.com/blog/the-history-of-hello-world/
///
/// Internals
/// =========
/// @see     https://www.intel.com/content/www/us/en/develop/documentation/cpp-compiler-developer-guide-and-reference/top/compiler-reference/macros/additional-predefined-macros.html
/// @see     
/// @see     https://gcc.gnu.org/onlinedocs/cpp/Standard-Predefined-Macros.html
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>

int main() {
	printf("Hello world!\n");

   printf("A fun fact about me:  I hate icebreakers. They're essentially in-person data scraping. Just inane exercises that extroverts feel are necessary to conduct a meeting. This meeting could have been an email, let's just move on.\n");

   printf("This program [%s] was compiled on [%s] at [%s]\n", __FILE__, __DATE__, __TIME__);

   printf("We are in the [%s] function right now.\n", __FUNCTION__);

#ifdef __INTEL_COMPILER
   printf("The Intel C++ compiler was used for compiling this program\n");
#elif __GNUC__
   printf("The GNU C Compiler version [%s] was used for compiling this program\n", __VERSION__);
#elif _MSC_VER
   printf("The compiler is Microsoft Visual Studio version [%d]\n", _MSC_VER);
#else
   printf("Can not determine the compiler\n");
#endif

   return 0;
}

